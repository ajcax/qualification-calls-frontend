import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: 'input[type=tel]'
})
export class PhoneDirective {

  @Input() maxLength: number = 10;

  constructor(private el: ElementRef) {
    this.maxLength = 10;
   }

  @HostListener('input', ['$event']) onInputChange(event: Event) {
    const input = event.target as HTMLInputElement;
    const onlyNumbers = input.value.replace(/[^\d]/g, '');
    input.value = onlyNumbers.slice(0, this.maxLength);
  }
}
