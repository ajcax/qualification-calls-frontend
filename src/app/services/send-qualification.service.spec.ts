import { TestBed } from '@angular/core/testing';

import { SendQualificationService } from './send-qualification.service';

describe('SendQualificationService', () => {
  let service: SendQualificationService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SendQualificationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
