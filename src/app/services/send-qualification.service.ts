import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '@env/environment';

@Injectable({
  providedIn: 'root'
})
export class SendQualificationService {

  private apiUrl: string;
  constructor(private http: HttpClient) {
    const host = environment.HOST;
    const port = environment.PORT;
    this.apiUrl = `http://${host}:${port}/api/save-qualification-call`;
  }

   sendFormData(data: any) {
    return this.http.post(this.apiUrl, data);
  }
}
