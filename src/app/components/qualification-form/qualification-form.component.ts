import { Component } from '@angular/core';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { SendQualificationService } from 'src/app/services/send-qualification.service';


@Component({
  selector: 'app-qualification-form',
  templateUrl: './qualification-form.component.html',
  styleUrls: ['./qualification-form.component.css']
})
export class QualificationFormComponent {

  constructor(private sendQualificationService: SendQualificationService, private _snackBar: MatSnackBar) {}

  selectedTag: string = '';

  tags: string[] = [
    'Back Office', 
    'Solicitud de servicio',
    'Servicio en curso', 
    'Baja', 
    'Cobertura',
    'Consulta Póliza',
    'Equivocado (nada en relación a IKÉ)', 
    'Reclamo Calidad (Reembolso/Daños...)'];

  qualificationForm = new FormGroup ({
    phone: new FormControl('', Validators.required),
    tags: new FormControl('', Validators.required),
  });


  isFormOk(data: any) {
    if(data.phone === '' || data.tags === '' || data.phone === null || data.tags === null) {
      return false
    }

    if(data.phone.length < 10) {
      return false
    }
    return true;
  }

  onSubmit() {
    const data = this.qualificationForm.value;
    let ok = this.isFormOk(data);
    const current_date = new Date();

    if (ok) {
      let payload = { ...data, current_date }
      this.sendQualificationService.sendFormData(payload).subscribe(
        response => {
          this._snackBar.open('Calificación guardada con exito!', 'Cerrar', {
            duration: 5000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: ['mat-mdc-snack-bar-container', 'snackbar-success']
          });
        },
        error => {
          this._snackBar.open('Lo sentimos, no se puedo guardar la calificación', 'Cerrar', {
            duration: 5000,
            horizontalPosition: 'right',
            verticalPosition: 'top',
            panelClass: ['mat-mdc-snack-bar-container', 'snackbar-error']
          });
        }
      )
    } else {
      this._snackBar.open('Por favor complete la información correctamente', 'Cerrar', {
        duration: 5000,
        horizontalPosition: 'right',
        verticalPosition: 'top',
        panelClass: ['mat-mdc-snack-bar-container', 'snackbar-error']
      });
    }
    this.qualificationForm.controls['phone'].markAsPristine();
    this.qualificationForm.controls['tags'].markAsPristine();
    this.qualificationForm.controls['phone'].updateValueAndValidity();
    this.qualificationForm.controls['tags'].updateValueAndValidity();
    this.qualificationForm.reset()
  }
}
